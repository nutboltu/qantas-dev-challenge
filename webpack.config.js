/* eslint-disable */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  filename: 'stylesheet.css',
});

module.exports = function(env) {
  return {
    context: path.resolve(__dirname, './src/client'),
    entry: path.resolve(__dirname,'./src/client/routes.jsx'),
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, './dist'),
      publicPath: '',
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              presets: ['react', 'env']
            },
          },
          include: [path.resolve(__dirname, './src')],
        },
        {
          test: /\.scss$/,
          loader: extractSass.extract({
            use: [{
              loader: 'css-loader',
            }, {
              loader: 'sass-loader',
            }],
          }),
        },
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [path.resolve(__dirname, './src'), 'node_modules'],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'index.tpl.html',
        inject: 'body',
        filename: 'index.html'
      }),
      extractSass,
    ]
  }
};
/* eslint-enable */
