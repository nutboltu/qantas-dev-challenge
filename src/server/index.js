import express from 'express';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import webpackConfig from '../../webpack.config';
import route from './routes';

const server = express();

server.use('/', route.router);

server.listen(8080, () => {
  /* eslint-disable */
  console.log('Server is running on port 8080');
});

const client = new WebpackDevServer(webpack(webpackConfig()));

client.listen(3000, 'localhost', () => {
  /* eslint-disable */
  console.log('App is running on port 3000');
});
