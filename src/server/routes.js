import express from 'express';
import graphqlHTTP from 'express-graphql';
import cors from 'cors';

import schema from './schemas';

const router = express.Router();

router.use('/graphql', cors(), graphqlHTTP({
  schema,
  graphiql: true,
}));

export default {
  router,
};
