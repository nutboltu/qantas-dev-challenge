import {
  GraphQLList,
  GraphQLObjectType,
  GraphQLSchema,
} from 'graphql';

import cmsDb from '../mockDb/cmsData.json';

import Home from './Home';
import Faq from './Faq';

const RootType = new GraphQLObjectType({
  name: 'AppSchema',
  fields: () => ({
    home: {
      type: Home,
      resolve: () => cmsDb.homepage,
    },
    faqs: {
      type: new GraphQLList(Faq),
      description: 'List of all faqs',
      resolve: () => cmsDb.faqs,
    },
  }),
});

const AppSchema = new GraphQLSchema({
  query: RootType,
});

export default AppSchema;
