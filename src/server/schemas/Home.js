import {
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';

const Home = new GraphQLObjectType({
  name: 'Home',
  fields: () => ({
    heading: { type: GraphQLString },
    subheading: { type: GraphQLString },
    heroImageUrl: { type: GraphQLString },
  }),
});

export default Home;
