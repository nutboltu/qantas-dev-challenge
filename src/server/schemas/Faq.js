import {
  GraphQLString,
  GraphQLObjectType,
} from 'graphql';

const Faq = new GraphQLObjectType({
  name: 'Faq',
  fields: () => ({
    title: { type: GraphQLString },
    body: { type: GraphQLString },
  }),
});

export default Faq;
