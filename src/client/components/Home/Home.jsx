import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  QueryRenderer,
  graphql,
} from 'react-relay';
import environment from '../../Environment';
import Loader from '../Loader';

const HomeQuery = graphql`
  query HomeQuery {
    home {
      heading
      subheading
      heroImageUrl
    }
  }
`;

const Home = () => (
  <QueryRenderer
    environment={environment}
    query={HomeQuery}
    render={({ error, props }) => {
      if (error) {
        return <div>{error.message}</div>;
      } else if (props) {
        const { home } = props;
        const homeStyle = {
          backgroundImage: `url(${home.heroImageUrl})`,
        };
        const HomeComponent = () => (
          <div className="home" style={homeStyle}>
            <div className="home-info">
              <div className="heading">{home.heading} </div>
              <div className="subheading">{home.subheading} </div>
              <Link to="/faqs" href="/faqs" className="learn-more"> Learn More </Link>
            </div>
          </div>
        );

        return HomeComponent();
      }
      return <Loader />;
    }}
  />
);

Home.propTypes = {
  home: PropTypes.shape({
    heading: PropTypes.string,
    subheading: PropTypes.string,
    heroImageUrl: PropTypes.string,
  }),
};

Home.defaultProps = {
  home: {
    heading: '',
    subheading: '',
    heroImageUrl: '',
  },
};

export default Home;

