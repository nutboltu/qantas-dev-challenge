import React from 'react';
import PropTypes from 'prop-types';

const Faq = (props) => {
  const bodyMarkup = () => ({ __html: props.body });
  return (
    <div className="faq-item">
      <span className="faq-item-title"> {props.title} </span>
      <p className="faq-item-body" dangerouslySetInnerHTML={bodyMarkup()} />
    </div>
  );
};

Faq.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
};

Faq.defaultProps = {
  title: '',
  body: '',
};

export default Faq;
