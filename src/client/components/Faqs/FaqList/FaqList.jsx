import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

const FaqList = (props) => {
  const { faqs } = props;
  return (
    <div>
      <div className="faq-list-heading"> Frequently Asked Questions </div>
      <div className="faq-list">
        {
          faqs.map((faq, index) =>
            (
              /* eslint-disable */
              <div
                id={index}
                key={uuid.v4()}
                className={ props.selectedIndex === index ? 'list-item active' : 'list-item' }
                onClick={props.onSelect}
              >
                {faq.title}
              </div>
            ))
        }
      </div>
    </div>
  );
};

FaqList.propTypes = {
  faqs: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    body: PropTypes.string,
  })),
  onSelect: PropTypes.func,
  selectedIndex: PropTypes.number,
};

FaqList.defaultProps = {
  faqs: [{
    title: '',
    body: '',
  }],
};

export default FaqList;
