import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  QueryRenderer,
  graphql,
} from 'react-relay';
import environment from '../../Environment';
import FaqList from './FaqList';
import Faq from './Faq';
import Loader from '../Loader';

const FaqsQuery = graphql`
  query FaqsQuery {
    faqs {
      title
      body
    }
  }
`;

class Faqs extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
    };
  }
  render() {
    return (
      <div>
        <QueryRenderer
          environment={environment}
          query={FaqsQuery}
          render={({ error, props }) => {
            if (error) {
              return <div>{error.message}</div>;
            } else if (props) {
              const { faqs } = props;
              const selectedFaq = faqs[this.state.selectedIndex];

              const onSelect = evt => this.setState({ selectedIndex: parseInt(evt.target.id, 10) });
              const FaqComponent = () => (
                <div className="faqs">
                  <Faq
                    title={selectedFaq.title}
                    body={selectedFaq.body}
                  />
                  <FaqList
                    faqs={faqs}
                    onSelect={onSelect}
                    selectedIndex={this.state.selectedIndex}
                  />
                </div>
              );

              return FaqComponent();
            }
            return <Loader />;
          }}
        />
      </div>
    );
  }
}

/* eslint-disable */
Faqs.propTypes = {
  faqs: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    body: PropTypes.string,
  })),
};

Faqs.defaultProps = {
  faqs: [{
    title: '',
    body: '',
  }],
};

export default Faqs;

