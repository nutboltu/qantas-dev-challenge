import React from 'react';
import { Route } from 'react-router-dom';
import Navigation from './Navigation';
import Home from '../Home';
import Faqs from '../Faqs';

const routes = [
  {
    href: '/',
    title: 'Home',
  },
  {
    href: '/faqs',
    title: 'Faqs',
  },
];

const App = () => (
  <div className="app">
    <Navigation items={routes} />
    <Route exact path="/" component={Home} />
    <Route path="/faqs" component={Faqs} />
  </div>
);

export default App;
