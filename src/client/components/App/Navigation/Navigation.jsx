import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { Link } from 'react-router-dom';

class Navigation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      className: 'navigation',
      selectedIndex: 0,
    };
    this.onResponsiveButtonClick = this.onResponsiveButtonClick.bind(this);
  }

  onResponsiveButtonClick() {
    const className = this.state.className === 'navigation' ?
      `${this.state.className} responsive` : 'navigation';
    this.setState({ className });
  }

  render() {
    return (
      <div className={this.state.className}>
        {
        this.props.items.map((item, index) => {
            const active = this.state.selectedIndex === index ? 'active' : '';
          return (
            <Link
              key={uuid.v4()}
              to={item.href}
              href={item.href}
              className={active}
              onClick={() => this.setState({ selectedIndex: index })}
            >
              {item.title}
            </Link>
          );
        })
      }
        <button className="icon" onClick={this.onResponsiveButtonClick}>&#9776;</button>
      </div>
    );
  }
}

Navigation.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    href: PropTypes.string,
    title: PropTypes.string,
  })),
};

Navigation.defaultProps = {
  items: [],
};

export default Navigation;
