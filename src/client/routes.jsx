import 'babel-polyfill';
import React from 'react';
import createBrowserHistory from 'history/createBrowserHistory';
import { render } from 'react-dom';
import { Router, Route } from 'react-router-dom';

import App from './components/App';

import './stylesheet.scss';

const history = createBrowserHistory();

render(
  <Router history={history}>
    <Route path="/" component={App} />
  </Router>,
  document.getElementById('root'),
);
