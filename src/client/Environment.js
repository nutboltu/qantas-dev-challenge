import {
  Environment,
  Network,
  RecordSource,
  Store,
} from 'relay-runtime';
const SERVER_ENDPOINT = 'http://localhost:8080/graphql';

function fetchQuery(
  operation,
  variables,
) {
  return fetch(SERVER_ENDPOINT, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => response.json());
}

const network = Network.create(fetchQuery);

const source = new RecordSource();
const store = new Store(source);

export default new Environment({
  network,
  store,
});

