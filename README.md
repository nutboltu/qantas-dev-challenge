## Qantas Developer Challenge

### Dependencies
1. React
2. Relay
3. GraphQL
4. Express

### How to Start

##### 1. Install all dependencies using npm or yarn

```npm i```

##### 2. Run relay compiler using following script

```npm run relay```

##### 3. Start the application

```npm start ```

Server will be running on http://localhost:8080/graphql (qraphiql tool is enabled.)

App will be running on http://localhost:3000

